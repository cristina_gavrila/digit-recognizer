var db = firebase.firestore();
 
function storeData() {
 
  var inputTitle = document.getElementById("articleTitle").value;
  var inputImageUrl = document.getElementById("articleImageUrl").value;
  var inputContent = document.getElementById("articleContent").value;
 
     db.collection("flutter_specifics").doc(Date.now().toString()).set({
         title: inputTitle,
         content: inputContent,
         imageUrl: inputImageUrl
     })
     .then(function() {
         console.log("Doc successful");
     })
     .catch(function(error) {
        console.error("Error writing doc", error);
     });
}