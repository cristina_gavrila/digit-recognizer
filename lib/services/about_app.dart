import 'package:DigitRecognizer/constants.dart';
import 'package:flutter/material.dart';

void customAboutDialog(BuildContext context) => showAboutDialog(
      context: context,
      applicationVersion: '1.0.0',
      applicationLegalese:
          "Application built by Gavrilă Cristina and Manole Daniela for the 'Medii și Instrumente de Programare' course. @2021",
      applicationIcon: Image.asset(
        Constants.logoImagePath,
        width: 60,
        height: 60,
      ),
    );
