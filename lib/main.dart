import 'package:DigitRecognizer/constants.dart';
import 'package:DigitRecognizer/screens/articles_screen.dart';
import 'package:DigitRecognizer/screens/drawing_screen.dart';
import 'package:DigitRecognizer/screens/home_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    App(),
  );
}

class App extends StatefulWidget {
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  bool _initialized = false;
  bool _error = false;

  void initializeFlutterFire() async {
    try {
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
      });
    } catch (e) {
      print(e);
      setState(() {
        _error = true;
      });
    }
  }

  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_error) {
      return Container();
    }

    if (!_initialized) {
      return CircularProgressIndicator();
    }

    return MaterialApp(
      title: 'Digit Recognizer',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.orange,
        accentColor: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeScreen(),
      routes: <String, WidgetBuilder>{
        "/home": (BuildContext context) => HomeScreen(),
        Constants.cardList[0].pageRoute: (BuildContext context) =>
            ArticlesScreen(
                collectionTitle: "flutter_specifics",
                pageTitle: Constants.cardList[0].title),
        Constants.cardList[1].pageRoute: (BuildContext context) =>
            ArticlesScreen(
                collectionTitle: "neural_networks",
                pageTitle: Constants.cardList[1].title),
        Constants.cardList[2].pageRoute: (BuildContext context) =>
            DrawingScreen(),
      },
    );
  }
}
