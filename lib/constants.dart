import 'package:DigitRecognizer/models/card.dart';

class Constants {
  static final canvasSize = 350.0;
  static final imageSize = 50.0;
  static final strokeWidth = 10.0;
  static final borderSize = 2;
  static final mnistImageSize = 28;
  static final cardSize = 250.0;
  static final bannerImagePath = "assets/banner.png";
  static final logoImagePath = "assets/logo.png";
  static final List<CardInfo> cardList = [
    CardInfo("Flutter Specifics", "What is Flutter and why should you use it?",
        "/flutter_specifics", "assets/flutter.png"),
    CardInfo("Neural Networks", "Is it that complicated?", "/neural_networks",
        "assets/neuralnet.png"),
    CardInfo(
        "Digit Recognizer",
        "Brought to you by Flutter, TFLite and digital evolution",
        "/digit_recognizer",
        "assets/demo.png")
  ];
}
