import 'package:flutter/foundation.dart';

class Prediction {
  final double confidence;
  final int index;
  final String label;

  Prediction({@required this.confidence, this.index, this.label});

  factory Prediction.fromJson(Map<dynamic, dynamic> json) {
    return Prediction(
      confidence: json['confidence'],
      label: json['label'],
      index: json['index'],
    );
  }
}
