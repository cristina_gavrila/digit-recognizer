class CardInfo {
  final String title;
  final String description;
  final String pageRoute;
  final String imagePath;

  CardInfo(this.title, this.description, this.pageRoute, this.imagePath);
}
