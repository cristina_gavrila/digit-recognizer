import 'package:DigitRecognizer/constants.dart';
import 'package:DigitRecognizer/models/prediction.dart';
import 'package:DigitRecognizer/screens/preferences_screen.dart';
import 'package:DigitRecognizer/services/recognizer.dart';
import 'package:DigitRecognizer/widgets/canvas.dart';
import 'package:DigitRecognizer/widgets/prediction.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrawingScreen extends StatefulWidget {
  @override
  _DrawingScreenState createState() => _DrawingScreenState();
}

class _DrawingScreenState extends State<DrawingScreen> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final _points = List<Offset>();
  final _recognizer = Recognizer();
  List<Prediction> _prediction;

  bool _disableButtons = true;

  @override
  void initState() {
    super.initState();
    _initModel();
  }

  void _initModel() async {
    await _recognizer.loadModel();
    setState(() {
      _disableButtons = true;
    });
  }

  void _recognize() async {
    List<dynamic> pred = await _recognizer.recognize(_points);
    setState(() {
      _prediction = pred.map((json) => Prediction.fromJson(json)).toList();
      _disableButtons = false;
    });
  }

  Future<void> _incrementRounds() async {
    final SharedPreferences prefs = await _prefs;

    final int rounds = (prefs.getInt('rounds') ?? 0) + 1;
    prefs.setInt("rounds", rounds).then((bool success) {});
  }

  Future<void> _incrementGuessed() async {
    if (_canvasHasValue()) {
      final SharedPreferences prefs = await _prefs;

      final int guessed = (prefs.getInt('guessed') ?? 0) + 1;
      prefs.setInt("guessed", guessed).then((bool success) {});
    }
  }

  bool _canvasHasValue() {
    return _points != null && _prediction != null;
  }

  void _clearCanvas() async {
    if (_canvasHasValue()) await _incrementRounds();
    setState(() {
      _points.clear();
      _prediction.clear();

      _disableButtons = true;
    });
  }

  Widget _canvasWidget() => Container(
        height: Constants.canvasSize,
        width: Constants.canvasSize,
        decoration: BoxDecoration(
          color: Colors.black,
        ),
        child: GestureDetector(
          onPanUpdate: (DragUpdateDetails details) {
            Offset _localPosition = details.localPosition;
            setState(() {
              _points.add(_localPosition);
            });
          },
          onPanEnd: (DragEndDetails details) {
            _points.add(null);
            _recognize();
          },
          child: CustomPaint(
            painter: CustomCanvas(_points),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    Widget _actionButton(color, icon, function) => Ink(
          decoration: ShapeDecoration(
            color: color,
            shape: CircleBorder(),
          ),
          child: IconButton(
            icon: Icon(icon),
            color: Colors.white,
            onPressed: function,
          ),
        );
    return Scaffold(
      appBar:
          AppBar(title: Text(Constants.cardList[2].title), actions: <Widget>[
        IconButton(
            icon: Icon(Icons.leaderboard),
            onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => PreferencesScreen()))),
      ]),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 10),
            _canvasWidget(),
            PredictionWidget(predictions: _prediction),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _actionButton(Colors.red, Icons.close,
              _disableButtons ? null : () => _clearCanvas()),
          SizedBox(
            width: 16,
          ),
          _actionButton(
            Colors.green,
            Icons.check,
            _disableButtons
                ? null
                : () async {
                    _clearCanvas();
                    await _incrementGuessed();
                  },
          ),
        ],
      ),
    );
  }
}
