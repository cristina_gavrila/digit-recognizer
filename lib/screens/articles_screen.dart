import 'package:DigitRecognizer/widgets/article_tile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ArticlesScreen extends StatefulWidget {
  final String collectionTitle;
  final String pageTitle;
  ArticlesScreen(
      {Key key, @required this.collectionTitle, @required this.pageTitle})
      : super(key: key);

  @override
  _ArticlesScreenState createState() => _ArticlesScreenState();
}

class _ArticlesScreenState extends State<ArticlesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.pageTitle)),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection(widget.collectionTitle)
            .snapshots(),
        builder:
            (BuildContext ctx, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasError) return Container();
          if (streamSnapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }
          if (streamSnapshot.data.docs.length == 0) {
            return Container();
          }
          return ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: streamSnapshot.data.docs.length,
            itemBuilder: (context, index) {
              return ArticleTile(article: streamSnapshot.data.docs[index]);
            },
          );
        },
      ),
    );
  }
}
