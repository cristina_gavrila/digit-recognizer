import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesScreen extends StatefulWidget {
  @override
  _PreferencesScreenState createState() => _PreferencesScreenState();
}

class _PreferencesScreenState extends State<PreferencesScreen> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  var _buttonColor = Colors.red;

  Future<int> _rounds;
  Future<int> _guessed;

  @override
  void initState() {
    super.initState();
    updateSharedState();
  }

  updateSharedState() {
    setState(() {
      _rounds = _prefs.then((SharedPreferences prefs) {
        return (prefs.getInt('rounds') ?? 0);
      });

      _guessed = _prefs.then((SharedPreferences prefs) {
        return (prefs.getInt('guessed') ?? 0);
      });
    });
  }

  Future<void> _clearData() async {
    final SharedPreferences preferences = await _prefs;

    preferences.setInt('rounds', 0);
    preferences.setInt('guessed', 0);

    updateSharedState();
  }

  @override
  Widget build(BuildContext context) {
    Widget _statsDetail(label, value) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Text(label),
              ),
              Text(
                value.toString(),
              ),
            ],
          ),
        );

    Widget _statsValues(guessed, rounds) => Column(
          children: [
            _statsDetail("Guessed", guessed),
            _statsDetail("Rounds", rounds),
            _statsDetail(
                "Accuracy", (100 * guessed / rounds).toStringAsFixed(2)),
          ],
        );

    Widget _buttonDetail() => Text(
          "Clear data",
          style: TextStyle(color: Colors.white),
        );

    bool isIOS = Theme.of(context).platform == TargetPlatform.iOS;

    Widget _clearButton() => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                child: isIOS
                    ? CupertinoButton(
                        color: _buttonColor,
                        onPressed: () {
                          _clearData();
                        },
                        child: _buttonDetail(),
                      )
                    : RaisedButton(
                        color: _buttonColor,
                        onPressed: () {
                          _clearData();
                        },
                        child: _buttonDetail(),
                      ),
              ),
            ],
          ),
        );

    Widget _stats = FutureBuilder(
      future: Future.wait([_guessed, _rounds]),
      builder: (BuildContext context, AsyncSnapshot<List<int>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return const CircularProgressIndicator();
          default:
            if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            } else {
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  _statsValues(snapshot.data[0], snapshot.data[1]),
                  _clearButton(),
                ],
              );
            }
        }
      },
    );

    return Scaffold(
        appBar: AppBar(
          title: Text("Stats"),
        ),
        body: Container(child: _stats));
  }
}
