import 'dart:ui';

import 'package:DigitRecognizer/services/about_app.dart';
import 'package:DigitRecognizer/widgets/app_drawer.dart';
import 'package:flutter/material.dart';
import 'package:particles_flutter/particles_flutter.dart';
import 'package:DigitRecognizer/constants.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ScrollController scrollController;
  int currentCardIndex = 0;

  @override
  void initState() {
    super.initState();
    scrollController = new ScrollController();
  }

  void _swipeCard(DragEndDetails details) {
    if (details.velocity.pixelsPerSecond.dx > 0) {
      if (currentCardIndex > 0) {
        currentCardIndex--;
      }
    } else {
      if (currentCardIndex < Constants.cardList.length - 1) {
        currentCardIndex++;
      }
    }
    setState(() {
      scrollController.animateTo((currentCardIndex) * (Constants.cardSize + 8),
          duration: Duration(milliseconds: 500), curve: Curves.fastOutSlowIn);
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    Widget _aboutApp = Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: IconButton(
        icon: Icon(Icons.help_outline),
        onPressed: () => customAboutDialog(context),
      ),
    );

    Widget _pageBackground = CircularParticle(
      awayRadius: 80,
      numberOfParticles: 200,
      speedOfParticles: 0.6,
      height: screenHeight,
      width: screenWidth,
      onTapAnimation: false,
      particleColor: Colors.black.withAlpha(150),
      awayAnimationDuration: Duration(milliseconds: 600),
      maxParticleSize: 8,
      isRandSize: true,
      isRandomColor: true,
      randColorList: [
        Theme.of(context).primaryColor.withAlpha(210),
        Theme.of(context).accentColor.withAlpha(210),
      ],
      awayAnimationCurve: Curves.easeInOutBack,
      enableHover: true,
      hoverColor: Colors.white,
      hoverRadius: 90,
      connectDots: true,
    );

    Widget _bannerImage = Container(
      child: Image.asset(Constants.bannerImagePath),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
      ),
    );

    Widget _cardBackground = Card(
      color: Theme.of(context).primaryColor.withOpacity(0.90),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: Center(
        child: Container(
          width: Constants.cardSize,
        ),
      ),
    );

    Widget _cardImage(position) => Center(
          child: Opacity(
            opacity: 0.8,
            child: Image.asset(
              Constants.cardList[position].imagePath,
              width: Constants.cardSize - 100,
            ),
          ),
        );

    Widget _cardDetail(position) => Container(
          alignment: AlignmentDirectional.bottomStart,
          width: Constants.cardSize,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Opacity(
                  opacity: .8,
                  child: Text(
                    Constants.cardList[position].description,
                    textAlign: TextAlign.left,
                  ),
                ),
                Text(
                  Constants.cardList[position].title,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.left,
                ),
              ],
            ),
          ),
        );

    Widget _card(position) => GestureDetector(
          onTap: () {
            Navigator.of(context)
                .pushNamed(Constants.cardList[position].pageRoute);
          },
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 50.0, horizontal: 16.0),
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: [
                _cardBackground,
                _cardImage(position),
                _cardDetail(position)
              ],
            ),
          ),
          onHorizontalDragEnd: (details) => _swipeCard(details),
        );

    Widget _cardList = Expanded(
      child: Container(
        child: ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          itemCount: Constants.cardList.length,
          controller: scrollController,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, position) {
            return _card(position);
          },
        ),
      ),
    );

    return Scaffold(
      drawer: AppDrawer(),
      appBar: AppBar(
        title: Text("HomeScreen"),
        actions: <Widget>[_aboutApp],
      ),
      body: Stack(
        children: <Widget>[
          _pageBackground,
          Column(
            children: [_bannerImage, _cardList],
          ),
        ],
      ),
    );
  }
}
