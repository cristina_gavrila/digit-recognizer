import 'package:DigitRecognizer/constants.dart';
import "package:flutter/material.dart";
import "package:flutter/rendering.dart";

class CustomCanvas extends CustomPainter {
  final List<Offset> points;

  CustomCanvas(this.points);

  final Paint _paint = Paint()
    ..strokeCap = StrokeCap.round
    ..color = Colors.white
    ..strokeWidth = Constants.strokeWidth;

  @override
  void paint(Canvas canvas, Size size) {
    for (var point = 0; point < points.length - 1; ++point) {
      if (points[point] != null && points[point + 1] != null) {
        canvas.drawLine(points[point], points[point + 1], _paint);
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter canvas) {
    return true;
  }
}
