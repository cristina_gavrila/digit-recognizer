import 'package:DigitRecognizer/constants.dart';
import 'package:DigitRecognizer/services/about_app.dart';
import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 160,
            child: DrawerHeader(
              child: null,
              decoration: BoxDecoration(color: Theme.of(context).primaryColor),
            ),
          ),
          Expanded(
            child: Column(
              children: [
                ListTile(
                  leading:
                      Image.asset(Constants.cardList[0].imagePath, height: 25),
                  title: Text(Constants.cardList[0].title),
                  onTap: () {
                    Navigator.of(context)
                        .popAndPushNamed(Constants.cardList[0].pageRoute);
                  },
                ),
                ListTile(
                  leading:
                      Image.asset(Constants.cardList[1].imagePath, height: 25),
                  title: Text(Constants.cardList[1].title),
                  onTap: () {
                    Navigator.of(context)
                        .popAndPushNamed(Constants.cardList[1].pageRoute);
                  },
                ),
                ListTile(
                  leading:
                      Image.asset(Constants.cardList[2].imagePath, height: 25),
                  title: Text(Constants.cardList[2].title),
                  onTap: () {
                    Navigator.of(context)
                        .popAndPushNamed(Constants.cardList[2].pageRoute);
                  },
                ),
              ],
            ),
          ),
          Container(
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: Column(
                children: [
                  Divider(),
                  ListTile(
                    leading: Icon(Icons.help_outline),
                    title: Text("About App"),
                    onTap: () => customAboutDialog(context),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
